import axios from "axios";
import Cookies from "js-cookie";

export const refreshToken = () => {
    return axios
        .post("token/refresh", {})
        .then((response) => {
            console.log("got the new token");
            localStorage.setItem("userToken", response.data.accessToken);
            return response;
        })
        .catch((err) => {
            console.log(err.response);
            return err.response;
        });
};

export const savePost = (userEmail, jobInfo, postId) => {
    return axios
        .post(
            "admin/savepost",
            {
                id: postId,
                email: userEmail,
                company: jobInfo.company,
                title: jobInfo.title,
                date: jobInfo.date,
                location: jobInfo.location,
                summary: jobInfo.summary,
            },
            {
                headers: {
                    Authorization: `Bearer ${localStorage.userToken}`,
                },
            }
        )
        .then((response) => {
            console.log("axios:: saved post");
            return response;
        })
        .catch((err) => {
            // console.log(err.response);
            return err.response;
        });
};

export const removePost = (userEmail, jobInfo, postId) => {
    console.log("axios: " + jobInfo);
    return axios
        .post(
            "admin/removepost",
            {
                id: postId,
                email: userEmail,
                company: jobInfo.company,
                title: jobInfo.title,
                date: jobInfo.date,
                location: jobInfo.location,
                summary: jobInfo.summary,
            },
            {
                headers: {
                    Authorization: `Bearer ${localStorage.userToken}`,
                },
            }
        )
        .then((response) => {
            console.log("axios:: removed post");
            return response;
        })
        .catch((err) => {
            // console.log(err.response);
            return err.response;
        });
};

export const getBookmarks = (userEmail) => {
    return axios
        .post(
            "admin/getbookmarks",
            {
                email: userEmail,
            },
            {
                headers: {
                    Authorization: `Bearer ${localStorage.userToken}`,
                },
            }
        )
        .then((response) => {
            console.log("axios:: get all the bookmarks");
            return response;
        })
        .catch((err) => {
            // console.log(err.response);
            return err.response;
        });
};

export const register = (newUser) => {
    return axios
        .post("users/register", {
            first_name: newUser.first_name,
            last_name: newUser.last_name,
            email: newUser.email,
            password: newUser.password,
        })
        .then((response) => {
            console.log("Registered");
            return response;
        });
};

export const login = (user) => {
    return axios
        .post("users/login", {
            email: user.email,
            password: user.password,
        })
        .then((response) => {
            localStorage.setItem("userToken", response.data.accessToken);
            return response.data.accessToken;
        })
        .catch((err) => {
            console.log(err);
        });
};

export const logout = () => {
    return axios
        .post("token/remove", {})
        .then((response) => {
            return response;
        })
        .catch((err) => {
            console.log(err);
        });
};
// function isExpired() {
//     // let expTime = parseFloat(localStorage.expTime);
//     // let currTimeInSec = Date.now() / 1000;
//     // console.log(expTime);
//     // console.log(currTimeInSec);
//     // let offset = expTime - currTimeInSec;

//     // if (currTimeInSec >= expTime - 30) {
//     console.log("Refreshing the token...\n" + localStorage.userToken);

//     refreshToken();
//     // }
// }
