import React from "react";

export default function Recentsearch() {
    return (
        <div className="my-1">
            <ul className="list-group list-group-horizontal d-flex align-items-center c-bg-white">
                <li className="list-group-item border-0 w-100">
                    <a className="c-black d-block" href="#">
                        <p>Chef</p>
                        <p className="text-secondary mb-0">San Jose</p>
                    </a>
                </li>
                <li className="list-group-item border-0">
                    <a className="c-black d-block" href="#">
                        <p>GO</p>
                    </a>
                </li>
            </ul>
        </div>
    );
}
