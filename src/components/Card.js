import React, { Component } from "react";
import jwt_decode from "jwt-decode";
import { withRouter } from "react-router-dom";
import {
    savePost,
    removePost,
    getBookmarks,
    refreshToken,
} from "./UserFunctions";

class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isBookmarked: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.isBookmarkedInDB = this.isBookmarkedInDB.bind(this);
        this.removeBookmark = this.removeBookmark.bind(this);
    }

    componentDidMount() {
        const userToken = localStorage.userToken;
        if (userToken) {
            this.setState({
                isBookmarked: this.props.bookmarked,
            });
        }
    }

    handleChange(e) {
        const { name, type, checked } = e.target;
        if (type === "checkbox") {
            this.setState({
                [name]: checked,
            });
            if (!this.state.isBookmarked) {
                this.bookmarkPost();
            } else {
                this.removeBookmark();
            }
            let tmp = "hello world";
            this.props.handleChangeCB(
                this.props.info,
                !this.state.isBookmarked
            );
        }
    }

    bookmarkPost() {
        let jobInfo = this.props.info;
        let postId = this.props.info.id;
        const userToken = localStorage.userToken;
        const user = jwt_decode(userToken);
        // console.log("userEmail: " + user.identity.email);
        savePost(user.identity.email, jobInfo, postId).then((res) => {
            if (res.status == 401) {
                console.log(
                    "401 error, token expired, requesting new token..."
                );
                refreshToken().then((res) => {
                    if (res.status == 401) {
                        console.log(
                            "401 error, refresh token expired. Logging out"
                        );
                        localStorage.removeItem("userToken");
                        this.props.history.push(`/`);
                        alert("You have been logged out. Please re login");
                    }
                });
                savePost(user.identity.email, jobInfo, postId);
            } else if (!res.error) {
                console.log("client: post saved" + res.data);
            }
        });
    }

    removeBookmark() {
        let jobInfo = this.props.info;
        let postId = this.props.info.id;
        const userToken = localStorage.userToken;
        const user = jwt_decode(userToken);
        // console.log("userEmail: " + user.identity.email);
        removePost(user.identity.email, jobInfo, postId).then((res) => {
            if (res.status == 401) {
                console.log(
                    "401 error, token expired, requesting new token..."
                );
                refreshToken().then((res) => {
                    if (res.status == 401) {
                        console.log(
                            "401 error, refresh token expired. Logging out"
                        );
                        localStorage.removeItem("userToken");
                        this.props.history.push(`/`);
                        alert("You have been logged out. Please re login");
                    }
                });
                removePost(user.identity.email, jobInfo, postId).then((res) => {
                    if (!res.error) {
                        this.setState({
                            isBookmarked: false,
                        });
                    }
                });
            } else if (!res.error) {
                console.log("client: post removed" + res.data);
                this.setState({
                    isBookmarked: false,
                });
            }
        });
    }

    isBookmarkedInDB() {
        const userToken = localStorage.userToken;
        const user = jwt_decode(userToken);
        let jobInfo = this.props.info;
        // console.log("userEmail: " + user.identity.email);
        getBookmarks(user.identity.email).then((res) => {
            if (res.status == 401) {
                console.log(
                    "401 error, token expired, requesting new token..."
                );
                refreshToken().then((res) => {
                    if (res.status == 401) {
                        console.log(
                            "401 error, refresh token expired. Logging out"
                        );
                        localStorage.removeItem("userToken");
                        this.props.history.push(`/`);
                        alert("You have been logged out. Please re login");
                    }
                });
                getBookmarks(user.identity.email).then((res) => {
                    if (!res.error) {
                        if (res.data.hasOwnProperty("bookmarks")) {
                            let bookmarks = res.data.bookmarks;

                            for (let i = 0; i < bookmarks.length; i++) {
                                let bmJob = bookmarks[i];

                                if (
                                    bmJob["title"] == jobInfo["title"] &&
                                    bmJob["company"] == jobInfo["company"] &&
                                    bmJob["date"] == jobInfo["date"] &&
                                    bmJob["location"] == jobInfo["location"]
                                ) {
                                    this.setState({
                                        isBookmarked: true,
                                    });
                                    break;
                                }
                            }
                        }

                        console.log(
                            "client: got all the bookmarks in cards.js "
                        );
                    }
                });
            } else if (!res.error) {
                if (res.data.hasOwnProperty("bookmarks")) {
                    let bookmarks = res.data.bookmarks;

                    for (let i = 0; i < bookmarks.length; i++) {
                        let bmJob = bookmarks[i];

                        if (
                            bmJob["title"] == jobInfo["title"] &&
                            bmJob["company"] == jobInfo["company"] &&
                            bmJob["date"] == jobInfo["date"] &&
                            bmJob["location"] == jobInfo["location"]
                        ) {
                            this.setState({
                                isBookmarked: true,
                            });
                            break;
                        }
                    }
                }

                console.log("client: got all the bookmarks in card.js ");
            }
        });
    }
    render() {
        let showStar = null;
        if (localStorage.userToken) {
            if (this.state.isBookmarked) {
                showStar = (
                    <i
                        className="fas fa-star fa-lg"
                        style={{ color: "#007bff" }}
                    ></i>
                );
            } else {
                showStar = (
                    <i
                        className="far fa-star fa-lg"
                        style={{ color: "#007bff" }}
                    ></i>
                );
            }
        }
        let checkboxId = this.props.info.id;
        return (
            <div className="card border-dark m-3" style={{ maxWidth: "18rem" }}>
                <div className="card-header d-flex justify-content-between">
                    {this.props.info.company}

                    <input
                        type="checkbox"
                        className="star-checkbox"
                        id={checkboxId}
                        name="isBookmarked"
                        checked={this.state.isBookmarked}
                        onChange={this.handleChange}
                    />
                    <label htmlFor={checkboxId}>{showStar}</label>
                </div>
                <div className="card-body text-dark d-flex flex-column justify-content-between">
                    <div>
                        <h5 className="card-title">{this.props.info.title}</h5>
                        <p className="card-text">{this.props.info.date}</p>
                        <p className="card-text">{this.props.info.location}</p>
                        <p className="card-text">{this.props.info.summary}</p>
                    </div>
                    <div>
                        <a
                            href={this.props.info.link}
                            target="_blank"
                            className="btn btn-primary"
                        >
                            Apply
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(Card);
