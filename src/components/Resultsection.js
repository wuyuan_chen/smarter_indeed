import React, { Component } from "react";
import Card from "./Card";
import jwt_decode from "jwt-decode";
import { withRouter } from "react-router-dom";
import { getBookmarks, refreshToken } from "./UserFunctions";
import { post } from "jquery";

class Resultsection extends Component {
    constructor() {
        super();
        this.state = {
            bookmarkedJobs: [],
            jobPosts: [],
            isLoading: true,
            bookmarkUpdateFlag: false,
        };
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        let jobPostArr = this.props.jobPosts;
        jobPostArr = jobPostArr.map(function (post) {
            //let jobPost = JSON.stringify(post);
            // return JSON.parse(jobPost);
            return post;
        });

        this.setState({
            jobPosts: jobPostArr,
        });

        const userToken = localStorage.userToken;
        if (userToken) {
            const user = jwt_decode(userToken);
            getBookmarks(user.identity.email).then((res) => {
                if (res.status == 401) {
                    console.log(
                        "401 error, token expired, requesting new token..."
                    );
                    refreshToken().then((res) => {
                        if (res.status == 401) {
                            console.log(
                                "401 error, refresh token expired. Logging out"
                            );
                            localStorage.removeItem("userToken");
                            this.props.history.push(`/`);
                            alert("You have been logged out. Please re login");
                        }
                    });
                    getBookmarks(user.identity.email).then((res) => {
                        if (!res.error) {
                            if (res.data.hasOwnProperty("bookmarks")) {
                                let bookmarkList = res.data.bookmarks;
                                this.setState({
                                    bookmarkedJobs: bookmarkList,
                                    isLoading: false,
                                });
                            }

                            console.log(
                                "client: got all the bookmarks in Resultsection.js"
                            );
                        }
                    });
                } else if (!res.error) {
                    // console.log(jobInfo);
                    // console.log(res.data);

                    if (res.data.hasOwnProperty("bookmarks")) {
                        let bookmarkList = res.data.bookmarks;
                        this.setState({
                            bookmarkedJobs: bookmarkList,
                            isLoading: false,
                        });
                    }

                    console.log(
                        "client: got all the bookmarks in Resultsection.js"
                    );
                }
            });
        } else {
            this.setState({
                isLoading: false,
            });
        }
    }
    handleChange(jobInfo, bmFlag) {
        console.log("reach handle change call back..... " + bmFlag);
        console.log(jobInfo);
        // this.forceUpdate();
        if (bmFlag) {
            this.setState((preState) => {
                preState.bookmarkedJobs.push(jobInfo);
                return {
                    bookmarkedJobs: preState.bookmarkedJobs,
                };
            });
        } else {
            this.setState((preState) => {
                const found = preState.bookmarkedJobs.find(
                    (post) => post === jobInfo
                );
                console.log("---> " + found);
                preState.bookmarkedJobs.splice(found, 1);
                return {
                    bookmarkedJobs: preState.bookmarkedJobs,
                };
            });
        }
    }
    render() {
        console.log("current state--> ");
        console.log(this.state.isLoading);
        // let jobPostArr = this.props.jobPosts;
        // jobPostArr = jobPostArr.map(function (post) {
        //     let jobPost = JSON.stringify(post);
        //     return JSON.parse(jobPost);
        // });
        if (this.state.isLoading) {
            return <h1> loading... </h1>;
        }
        let jobPostArr = this.state.jobPosts;
        let dbJobs = this.state.bookmarkedJobs;
        let bookmarkedIds = [];
        if (dbJobs.length) {
            bookmarkedIds = dbJobs.map(function (bm) {
                //let jobPost = JSON.stringify(post);
                // return JSON.parse(jobPost);
                return bm.id;
            });
        }

        let savedJobsDisplay;
        if (dbJobs.length) {
            savedJobsDisplay = (
                <div className="d-flex justify-content-center flex-wrap">
                    {dbJobs.map(function (jobInfo, i) {
                        return (
                            <Card
                                info={jobInfo}
                                key={jobInfo.id}
                                id={jobInfo.id}
                                bookmarked={true}
                                handleChangeCB={this.handleChange}
                            />
                        );
                    }, this)}
                </div>
            );
        } else {
            savedJobsDisplay = (
                <div className="d-flex justify-content-center flex-wrap">
                    you have no jobs saved.
                </div>
            );
        }
        // let jobPosts = JSON.parse(this.props.jobPosts[0]);
        return (
            <div className="my-3">
                <div>
                    <div
                        className="justify-content-center nav nav-tabs"
                        id="nav-tab"
                        role="tablist"
                    >
                        <a
                            className="c-black nav-item nav-link active"
                            id="nav-jobs-tab"
                            data-toggle="tab"
                            href="#nav-jobs"
                            role="tab"
                            aria-controls="nav-jobs"
                            aria-selected="true"
                        >
                            JOBS
                        </a>
                        <a
                            className="c-black nav-item nav-link"
                            id="nav-saved-tab"
                            data-toggle="tab"
                            href="#nav-profile"
                            role="tab"
                            aria-controls="nav-profile"
                            aria-selected="false"
                        >
                            SAVED
                        </a>
                        <a
                            className="c-black nav-item nav-link"
                            id="nav-alerts-tab"
                            data-toggle="tab"
                            href="#nav-contact"
                            role="tab"
                            aria-controls="nav-contact"
                            aria-selected="false"
                        >
                            ALERTS
                        </a>
                    </div>
                </div>
                <div className="tab-content" id="nav-tabContent">
                    <div
                        className="tab-pane fade show active"
                        id="nav-jobs"
                        role="tabpanel"
                        aria-labelledby="nav-jobs-tab"
                    >
                        <div className="d-flex justify-content-center flex-wrap">
                            {jobPostArr.map(function (jobInfo, i) {
                                let bookmarked = false;

                                if (bookmarkedIds.includes(jobInfo.id)) {
                                    bookmarked = true;
                                }
                                return (
                                    <Card
                                        info={jobInfo}
                                        key={jobInfo.id}
                                        id={jobInfo.id}
                                        bookmarked={bookmarked}
                                        handleChangeCB={this.handleChange}
                                    />
                                );
                            }, this)}
                        </div>
                    </div>
                    <div
                        className="tab-pane fade"
                        id="nav-profile"
                        role="tabpanel"
                        aria-labelledby="nav-saved-tab"
                    >
                        {savedJobsDisplay}
                    </div>
                    <div
                        className="tab-pane fade"
                        id="nav-contact"
                        role="tabpanel"
                        aria-labelledby="nav-alert-tab"
                    >
                        alerts
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(Resultsection);
