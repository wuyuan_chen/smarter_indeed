import React, { Component } from "react";
import { withRouter } from "react-router";

class Searchbar extends Component {
    constructor() {
        super();
        this.state = {
            jobTitle: "",
            location: "",
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    }

    handleSubmit(event) {
        console.log("submitted");
        console.log(this.state.jobTitle, this.state.location);
        // event.preventDefault();
        const titleStr = this.state.jobTitle.replace(" ", "+");
        const locationStr = this.state.location.replace(" ", "+");
        const queryStr = `title=${titleStr}&location=${locationStr}`;
        this.props.history.push(`/results?${queryStr}`);
    }

    render() {
        return (
            <form
                className="div-flex search-form"
                method="get"
                onSubmit={this.handleSubmit}
            >
                <div className="input-group input-group-lg">
                    <div className="input-group-prepend">
                        <span className="input-style" id="inputGroup-sizing-lg">
                            What
                        </span>
                    </div>
                    <input
                        type="text"
                        placeholder="Job title"
                        className="bl-none form-control"
                        aria-label="Large"
                        aria-describedby="inputGroup-sizing-sm"
                        name="jobTitle"
                        value={this.state.jobTitle}
                        onChange={this.handleChange}
                    />
                </div>
                <div className="margin-top input-group input-group-lg">
                    <div className="input-group-prepend">
                        <span className="input-style" id="inputGroup-sizing-lg">
                            Where
                        </span>
                    </div>
                    <input
                        type="text"
                        placeholder="location"
                        className="bl-none form-control"
                        aria-label="Large"
                        aria-describedby="inputGroup-sizing-sm"
                        name="location"
                        value={this.state.location}
                        onChange={this.handleChange}
                    />
                </div>
                <input
                    className="margin-top button-style btn btn-primary"
                    type="submit"
                    value="Find Jobs"
                />
            </form>
        );
    }
}

export default withRouter(Searchbar);
