import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { logout } from "./UserFunctions";
import jwt_decode from "jwt-decode";

class Navbar extends Component {
    constructor() {
        super();
        this.state = {
            first_name: "",
            last_name: "",
            email: "",
        };
    }

    componentDidMount() {
        const userToken = localStorage.userToken;
        if (userToken) {
            const decodedToken = jwt_decode(userToken);
            this.setState({
                first_name: decodedToken.identity.first_name,
                last_name: decodedToken.identity.last_name,
                email: decodedToken.identity.email,
            });
        }
    }

    signOut(e) {
        e.preventDefault();
        localStorage.removeItem("userToken");
        logout().then(() => {
            console.log("removed token from http cookie");
        });
        this.props.history.push(`/`);
    }

    render() {
        const firstName = this.state.first_name;
        const signInLink = <Link to="/signin">Sign In</Link>;
        const signOutLink = (
            <a className="c-black" href="#" onClick={this.signOut.bind(this)}>
                Hi {firstName}, Sign Out
            </a>
        );
        return (
            <nav className="ml-3 mr-3 navbar navbar-expand-lg navbar-light">
                <Link to="/">
                    <span className="navbar-brand">Smarter Indeed</span>
                </Link>

                <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div
                    className="collapse navbar-collapse"
                    id="navbarSupportedContent"
                >
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                                Find Jobs{" "}
                                <span className="sr-only">(current)</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                                Company Review
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                                Find Salaries
                            </a>
                        </li>
                    </ul>
                    {localStorage.userToken
                        ? console.log("signed in")
                        : console.log("signed out")}
                    {localStorage.userToken ? signOutLink : signInLink}
                </div>
            </nav>
        );
    }
}
export default withRouter(Navbar);
