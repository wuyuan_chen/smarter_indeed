import React, { Component } from "react";
import { Link } from "react-router-dom";
import { register } from "../components/UserFunctions";

class SignUp extends Component {
    constructor() {
        super();
        this.state = {
            first_name: "",
            last_name: "",
            email: "",
            password: "",
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value,
        });
    }

    onSubmit(e) {
        e.preventDefault();

        const newUser = {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            email: this.state.email,
            password: this.state.password,
        };

        register(newUser).then((res) => {
            this.props.history.push(`/signin`);
        });
    }

    render() {
        return (
            <div className="container h-100">
                <div className="row h-100">
                    <div className="col-sm-9 col-md-7 col-lg-5 m-auto">
                        <Link to="/">
                            <h3 className="text-center mb-3">Smarter Indeed</h3>
                        </Link>
                        <div className="card card-signin">
                            <div className="card-body">
                                <h5 className="card-title"> Sign Up</h5>
                                <form noValidate onSubmit={this.onSubmit}>
                                    <div className="form-group">
                                        <label htmlFor="first_name">
                                            First Name
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="first_name"
                                            placeholder="Last name"
                                            value={this.state.first_name}
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="last_name">
                                            Last Name
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="last_name"
                                            placeholder="Enter Last Name"
                                            value={this.state.last_name}
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="regEmailInput">
                                            Email address
                                        </label>
                                        <input
                                            type="email"
                                            className="form-control"
                                            id="regEmailInput"
                                            aria-describedby="emailHelp"
                                            placeholder="Enter email"
                                            name="email"
                                            value={this.state.email}
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="regPassword">
                                            Password
                                        </label>
                                        <input
                                            type="password"
                                            className="form-control"
                                            id="regPassword"
                                            placeholder="Password"
                                            name="password"
                                            value={this.state.password}
                                            onChange={this.onChange}
                                        />
                                    </div>

                                    <button
                                        type="submit"
                                        className="btn btn-primary w-100 round-border"
                                    >
                                        Register
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div className="text-center my-3">
                            <Link to="/signin">
                                Already a member? Sign in here
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SignUp;
