import React, { Component } from "react";

import queryString from "query-string";
import $ from "jquery";

import Navbar from "../components/Navbar";
import Searchbar from "../components/Searchbar";
import Resultsection from "../components/Resultsection";

class Result extends Component {
    constructor() {
        super();
        this.state = {
            jobPosts: {},
        };
    }

    componentDidMount() {
        const url = "https://frozen-tor-88375.herokuapp.com/";

        const param = queryString.parse(this.props.location.search);
        const data = {
            jobTitle: param.title,
            location: param.location,
        };

        // console.log("result--> " + JSON.stringify(param.location));
        const self = this;
        $.ajax({
            type: "get",
            url: "http://localhost:5000",
            //url: "https://frozen-tor-88375.herokuapp.com/",
            cache: false,
            async: "asynchronous",
            dataType: "json",
            data: data,
            success: function (data) {
                // console.log("-----> " + JSON.stringify(data.jobs));
                self.setState({
                    jobPosts: data.jobs,
                });

                // this is the function that uses the json object
                // and display the info on the web
                // callback(data);
            },
            error: function (request, status, error) {
                console.log("Error: " + error);
            },
        });
    }

    render() {
        let resultSection;
        // console.log("##-----> " + JSON.stringify(this.state.jobPosts));
        if (Object.keys(this.state.jobPosts).length == 0) {
            resultSection = <h1>loading...</h1>;
        } else {
            resultSection = <Resultsection jobPosts={this.state.jobPosts} />;
        }
        return (
            <div>
                <Navbar />
                <div className="container">
                    <Searchbar />
                    {resultSection}
                </div>
            </div>
        );
    }
}

export default Result;
