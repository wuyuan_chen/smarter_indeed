import React, { Component } from "react";
import { Link } from "react-router-dom";
import { login } from "../components/UserFunctions";

class SignIn extends Component {
    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value,
        });
    }
    onSubmit(e) {
        e.preventDefault();
        const user = {
            email: this.state.email,
            password: this.state.password,
        };

        login(user).then((res) => {
            if (!res.error) {
                this.props.history.push("/");
            }
        });
    }
    render() {
        return (
            <div className="container h-100">
                <div className="row h-100">
                    <div className="col-sm-9 col-md-7 col-lg-5 m-auto">
                        <Link to="/">
                            <h3 className="text-center mb-3">Smarter Indeed</h3>
                        </Link>
                        <div className="card card-signin">
                            <div className="card-body">
                                <h5 className="card-title"> Sign In</h5>
                                <form noValidate onSubmit={this.onSubmit}>
                                    <div className="form-group">
                                        <label htmlFor="inputEmail">
                                            Email address
                                        </label>
                                        <input
                                            type="email"
                                            className="form-control"
                                            id="inputEmail"
                                            aria-describedby="emailHelp"
                                            placeholder="Enter email"
                                            name="email"
                                            value={this.state.email}
                                            onChange={this.onChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="yourPassword">
                                            Password
                                        </label>
                                        <input
                                            type="password"
                                            className="form-control"
                                            id="yourPassword"
                                            placeholder="Password"
                                            name="password"
                                            value={this.state.password}
                                            onChange={this.onChange}
                                        />
                                    </div>

                                    <button
                                        type="submit"
                                        className="btn btn-primary w-100 round-border"
                                    >
                                        Sign In
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div className="text-center my-3">
                            <Link to="/signup">
                                Not a member? Create an account free
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SignIn;
