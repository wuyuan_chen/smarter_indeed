import React from "react";
import "../App.css";
import Navbar from "../components/Navbar";
import Recentsearch from "../components/Recentsearch";
import Searchbar from "../components/Searchbar";
import Suggestedsearch from "../components/Suggestedsearch";
function Landing() {
    return (
        <div className="showcase">
            <Navbar />
            <div className="height-div"></div>
            <div className="flex-col center h-100">
                <div className="container">
                    <Searchbar />
                    <div className="text-center mb-5">
                        <p className="mt-5">
                            <span>Post your resume</span> – It only takes a few
                            seconds
                        </p>
                        <p className="mt-3">
                            <span>Employers: get started</span> – post a job,
                            search resumes, and more
                        </p>
                    </div>
                </div>
                <div className="w-100 c-bg-gray">
                    <div className="d-flex flex-col container align-items-center">
                        <div className="width-div my-3">
                            <div className="d-flex flex-row justify-content-between align-items-center mb-3">
                                <p>Recent Search</p>
                                <p>Edit</p>
                            </div>
                            <Recentsearch />
                            <Recentsearch />
                            <Recentsearch />
                            <Recentsearch />
                            <Suggestedsearch />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Landing;
