import React from "react";
import "./App.css";
import { Switch, Route } from "react-router-dom";
import Landing from "./pages/Landing.js";
import Results from "./pages/Results.js";
import SignIn from "./pages/Sign_in.js";
import SignUp from "./pages/Sign_up.js";
import Error from "./pages/Error.js";

function App() {
    return (
        <>
            <Switch>
                <Route exact path="/" component={Landing} />
                <Route exact path="/results" component={Results} />
                <Route exact path="/signin" component={SignIn} />
                <Route exact path="/signup" component={SignUp} />
                <Route component={Error} />
            </Switch>
        </>
    );
}
export default App;
